# README #

Easy setup of a wemos D1 with DHT30 temperatur sensor expantion module

### What is this repository for? ###

* Quick summary
Code connects fetches the temperatur and sends it to blynk

* Version
This is version 1.0


### How do I get set up? ###

* Summary of set up
To get you wemos please see http://wemos.cc

* Dependencies

It uses the following libaries
- BlynkSimpleEsp8266.h
- SimpleTimer.h
- WEMOS_SHT3X.h

* Deployment instructions
I have used Arduino IDE 1.8.1


### Who do I talk to? ###

Handle: Nhimself
email: nikimench@gmail.com

### Licens and Credits ###

Copyright 2017 Niki Mench

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--- 
Arduino IDE is developed and maintained by the Arduino team. The IDE is licensed under GPL.

ESP8266 core includes an xtensa gcc toolchain, which is also under GPL.

Espressif SDK included in this build is under Espressif MIT License.

ESP8266 core files are licensed under LGPL.