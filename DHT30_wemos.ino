/*
 * Sketch build by Nhimself
 * email: nikimench@gmail.com
 * https://bitbucket.org/nhimself/wemos-with-dht30-and-blynk
 * 
 * This section is for you to edit before deployment
 */
char auth[] = "";
char ssid[] = "";
char pass[] = "";
int updateBlynk = 2000L; // in mill sec
int fetchTemp = 2000L;     // in mill sec
/*
 * End of config section
 */

/*----------------------------------------------------------------------------------*/

#define BLYNK_PRINT Serial
#include <BlynkSimpleEsp8266.h>
#include <SimpleTimer.h>
#include <WEMOS_SHT3X.h>

SHT3X sht30(0x45);
SimpleTimer timer;
float t;
float h;

void sendSensor()
{
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to get from DHT sensor!");
    return;
  }
  Blynk.virtualWrite(V1, t);
  Blynk.virtualWrite(V2, h);
}


void getTemp()
{
  sht30.get();
  h = sht30.humidity;
  t = sht30.cTemp;
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to get from DHT sensor!");
    return;
  }
  Serial.print("Temperature: ");
  Serial.println(t);
  Serial.print("Relative Humidity: ");
  Serial.println(h);
  Serial.println();
}

void setup()
{
  Serial.begin(115200); 
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(updateBlynk, sendSensor);
  timer.setInterval(fetchTemp, getTemp);
}

void loop()
{
  Blynk.run(); // Initiates Blynk
  timer.run(); // Initiates SimpleTimer
}

